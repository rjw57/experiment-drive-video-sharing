# Video sharing engine

## Further documentation

Usage and API documentation may be built using tox:

```bash
$ COMPOSE_ARGS="-v $PWD/build/:/tmp/tox-data/artefacts/" ./tox.sh -e doc
$ xdg-open build/doc/index.html   # Linux
$ open build/doc/index.html       # Mac
```

## Quickstart

> This quickstart is an abbreviated form of the getting started guide from the
> main documentation.

The ``./vidshare_development.sh`` script will build a containerised version of
the tool and run it with the repository directory mounted read-only under
``/usr/src/app`` inside the container. As such you can have development-local
configuration inside the repository.

When *first running* the tool, you will need to create some configuration. (See
the "Configuration" section in the documentation for what is required.)

```bash
$ cd /path/to/this/repo
$ mkdir .vidshare
$ cp configuration-template.yaml .vidshare/configuration.yaml
# ... edit configuration, see below ...
```

Once configured, the vidshare can be run as follows:

```bash
$ ./vidshare_development.sh
```

> The ``vidshare_development.sh`` script can be used to run the vidshare utility
> within a development Docker image. The repository directory is mounted read-only
> as ``/usr/src/app`` so that local modifications take effect.

## Running tests

The tests may be run using tox:

```bash
$ ./tox.sh
```
