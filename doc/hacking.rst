Overview for Developers
=======================

The  application is naturally concurrent in that it repeatedly
interacts with network APIs and has multiple sub-tasks running at the same time.
In order to simplify the concurrence, the vidshare uses the `asyncio module
<https://docs.python.org/3/library/asyncio.html>`_ from Python 3.
