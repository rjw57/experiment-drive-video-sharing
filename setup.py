import os

from setuptools import find_packages, setup


def load_requirements():
    """
    Load requirements file and return non-empty, non-comment lines with leading and trailing
    whitespace stripped.
    """
    with open(os.path.join(os.path.dirname(__file__), 'requirements.txt')) as f:
        return [
            line.strip() for line in f
            if line.strip() != '' and not line.strip().startswith('#')
        ]


setup(
    name='vidshare',
    install_requires=load_requirements(),
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'vidshare=vidshare.tool:main'
        ],
    },
)
