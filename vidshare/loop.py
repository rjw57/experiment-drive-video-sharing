"""
Main event loop.

"""
import asyncio
import logging
import sys

from . import googledrive

LOG = logging.getLogger(__name__)


def run(configuration):
    # Run main loop(s)
    asyncio.run(main_loop(configuration))

    # The main loop should never terminate
    LOG.error('Unexpected exit from main loop')
    sys.exit(1)


async def main_loop(configuration):
    """
    Coroutine which runs all the main loops concurrently until they all exit.

    """
    # A list of co-routines which represent ingest loops.
    ingest_loops = []

    # If there is configuration for the Google Sheets ingest, add the corresponding loop to the
    # list of ingest loops.
    if 'drive' in configuration:
        ingest_loops.append(googledrive.loop(
            configuration=googledrive.Configuration.from_dict(configuration['drive'])
        ))

    # Run long-lived ingest loops concurrently. Any one loop raising an exception will cause the
    # entire task to terminate with that exception. This ensures that things fail loud rather than
    # silently causing one of our task loops to be silently not running.
    return await asyncio.gather(*ingest_loops)
