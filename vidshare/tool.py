"""
Schedule Lecture Capture Recordings

Usage:
    vidshare (-h | --help)
    vidshare [(--quiet|--verbose)] [--configuration=PATH]

Options:

    -h, --help                  Show a brief usage summary.
    --quiet, -q                 Reduce logging verbosity.
    --verbose, -v               Increase logging verbosity.
    --configuration=PATH        Override location of configuration file (see below).

Configuration:

    Configuration of the tool is via a YAML document. The default location for the configuration is
    the first file which exists in the following locations:

    - The value of the VIDSHARE_CONFIGURATION environment variable
    - ./.vidshare/configuration.yaml
    - ~/.vidshare/configuration.yaml
    - /etc/vidshare/configuration.yaml

    The --configuration option may be used to override the search path.

"""
import logging
import sys

import docopt

from . import config, loop


LOG = logging.getLogger(__name__)


def main():
    opts = docopt.docopt(__doc__)

    log_level = logging.INFO
    if opts['--quiet']:
        log_level = logging.WARN
    elif opts['--verbose']:
        log_level = logging.DEBUG

    logging.basicConfig(level=log_level)

    # Load the configuration
    try:
        configuration = config.load_configuration(location=opts.get('--configuration'))
    except config.ConfigurationError as e:
        LOG.error('Could not load configuration: %s', e)
        sys.exit(1)

    try:
        loop.run(configuration)
    except Exception as e:
        LOG.error('error: %s', e, exc_info=e)
        sys.exit(1)
