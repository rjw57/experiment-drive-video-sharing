"""
Ingest of events from Google Sheets.

"""
import asyncio
import dataclasses
import json
import logging
import typing

import googleapiclient.discovery
import httplib2shim  # more modern httplib2 shim
from oauth2client.service_account import ServiceAccountCredentials

from .config import ConfigurationDataclassMixin

LOG = logging.getLogger(__name__)

# Scopes required to access Google spreadsheets
SCOPES = [
    'https://www.googleapis.com/auth/drive'
]

# Expected column headings
HEADINGS = [
    'id', 'cancelled', 'lecturer_crsids', 'start_at', 'duration_minutes', 'title', 'vle_url',
    'sequence_id', 'sequence_index', 'location_id', 'series_id'
]

#: Default delay (in seconds) between polls to the Drive API. Changes to Google Sheets are slow to
#: manifest themselves in the modification time so there is no advantage in this being any smaller
#: than around 5 minutes.
DEFAULT_POLL_DELAY = 60*5


class ParseError(RuntimeError):
    """An exception raised if the parsed Google sheet has an invalid format."""


@dataclasses.dataclass
class Configuration(ConfigurationDataclassMixin):
    """
    Configuration for the Google Sheets ingest.

    """
    # Path to JSON service account credentials
    service_account_credentials_path: str

    # How long to wait between polling Google API for changes
    poll_delay_seconds: typing.Union[int, float] = DEFAULT_POLL_DELAY


async def loop(*, configuration):
    """

    """
    # Load credentials
    LOG.info('Loading credentials from: %s', configuration.service_account_credentials_path)
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        configuration.service_account_credentials_path, SCOPES)

    # The Google discovery API client prints warnings about not being able to use a file cache
    # which is benign but includes an exception traceback which can confuse some log parsers. Set
    # the log level for that module to ERROR to silence them.
    logging.getLogger('googleapiclient.discovery_cache').setLevel(logging.ERROR)

    # Create a Google drive client
    drive_service = await _build_drive_service(credentials)
    LOG.info('Drive service: %r', drive_service)

    # Now we've built the service, reset the log level to NOTSET. Unfortunately, there's not an
    # official way of determining the actual log level of the logger to reset it to exactly what it
    # was before.
    logging.getLogger('googleapiclient.discovery_cache').setLevel(logging.NOTSET)

    change_token, _ = await _drive_list_changes(drive_service)

    query = "mimeType contains 'video/'"
    _log_json(await _drive_list_files(
        drive_service, query=query, extra_fields=['files(id,name,permissionIds)']
    ))

    while True:
        change_token, changes = await _drive_list_changes(
            drive_service, start_token=change_token, extra_fields=['*'])
        _log_json(changes)

        # Wait for the next poll time.
        LOG.info('Sleeping for %s seconds', configuration.poll_delay_seconds)
        await asyncio.sleep(configuration.poll_delay_seconds)


async def _build_drive_service(credentials):
    """
    Co-routine which builds a Google Drive API service using the Google API client libraries. Runs
    the code within the default asyncio executor.

    """
    def build_service():
        http = httplib2shim.Http()
        return googleapiclient.discovery.build('drive', 'v3', http=credentials.authorize(http))
    return await asyncio.get_running_loop().run_in_executor(None, build_service)


async def _drive_list_files(drive_service, *, query=None, extra_fields=None):
    """
    Co-routine which uses the Google drive API to get a list of files. Automatically handles paging
    as necessary to retrieve a full list.

    """
    # Get the current asyncio event loop so that we can call the drive API in the loop's default
    # thread pool.
    loop = asyncio.get_running_loop()

    # List of file metadata resources to return
    files = []

    # List of fields in metadata to return
    fields = ','.join(extra_fields) if extra_fields is not None else None

    # Loop while we wait for nextPageToken to be "none"
    page_token = None
    while True:
        list_response = await loop.run_in_executor(
            None, drive_service.files().list(
                corpora='user,allTeamDrives', supportsTeamDrives=True, fields=fields,
                includeTeamDriveItems=True, pageToken=page_token, q=query
            ).execute
        )

        # Add returned files to response list
        files.extend(list_response.get('files', []))

        # Get the token for the next page
        page_token = list_response.get('nextPageToken')
        if page_token is None:
            break

    return files


async def _drive_list_changes(drive_service, *, start_token=None, extra_fields=None, **kwargs):
    """
    Co-routine which uses the Google drive API to get a list of files. Automatically handles paging
    as necessary to retrieve a full list.

    """
    # Get the current asyncio event loop so that we can call the drive API in the loop's default
    # thread pool.
    loop = asyncio.get_running_loop()

    # List of file metadata resources to return
    changes = []

    # List of fields in metadata to return
    fields = ','.join(extra_fields) if extra_fields is not None else None

    if start_token is None:
        start_token = (await loop.run_in_executor(
            None, drive_service.changes().getStartPageToken().execute)).get('startPageToken')
    LOG.info('Change list start token: %r', start_token)

    # Loop while we wait for nextPageToken to be "none"
    page_token = start_token
    while True:
        list_response = await loop.run_in_executor(
            None, drive_service.changes().list(
                supportsTeamDrives=True, fields=fields,
                includeTeamDriveItems=True, pageToken=page_token
            ).execute
        )

        # Add returned files to response list
        changes.extend(list_response.get('changes', []))

        if 'newStartPageToken' in list_response:
            start_token = list_response.get('newStartPageToken')

        # Get the token for the next page
        page_token = list_response.get('nextPageToken')
        if page_token is None:
            break

    return start_token, changes


def _log_json(value, message=None):
    if message is not None:
        LOG.debug('%s', message)
    for line in json.dumps(value, indent=2).splitlines():
        LOG.debug('%s', line)
