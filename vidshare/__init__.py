"""
The Drive Video Sharer is a concurrent application written using the Python
`asyncio library <https://docs.python.org/3/library/asyncio.html>`_.

Google Drive ingest
--------------------

.. automodule:: vidshare.googledrive
    :members:
    :private-members:
    :member-order: bysource

Main loop
---------

.. automodule:: vidshare.loop
    :members:
    :member-order: bysource

Utilities
---------

.. automodule:: vidshare.config
    :members:
    :member-order: bysource

"""
